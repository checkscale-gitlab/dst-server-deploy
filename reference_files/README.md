# Reference Files
Here are files which can be easily ingested by dst-server-deploy.



### Attribution
The ids used in the `adminlist.csv`, `whitelist.csv`, and `blacklist.csv` were taken from the fairplay-zone/docker-dontstarvetogether [configuration](https://github.com/fairplay-zone/docker-dontstarvetogether/blob/develop/docs/configuration.md) page. 
